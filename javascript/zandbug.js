jQuery(document).ready(function($){
	"use strict";
	/* Caret Position */
    $.fn.selectRange = function(start, end) {
        if(!end) end = start; 
        return this.each(function() {
            if (this.setSelectionRange) {
                this.focus();
                this.setSelectionRange(start, end);
            } else if (this.createTextRange) {
                var range = this.createTextRange();
                range.collapse(true);
                range.moveEnd('character', end);
                range.moveStart('character', start);
                range.select();
            }
        });
    };				
	/* Editeur css */	
    document.addEventListener('keydown', function (event) {
        var esc = event.which == 27,
        el = event.target,
        input = el.nodeName != 'INPUT' && el.nodeName != 'TEXTAREA',
        data = {};	
		$('#go-style').click(function() {				
			event.preventDefault();
			data = el.innerHTML;
			var myJSON = data;
			el.blur();
			event.preventDefault();
			$.ajax({
				global: false,
				type: "POST",
				cache: false,
				dataType: "json",
				data: ({
					action: 'write',
					config: myJSON
				}),
				url: 'exec/exec-style-editor.php'
			});
			$(function() {
				$('#editor').append(function(e) {
					$.ajax({
						global: false,
						type: "POST",
						cache: false,
						dataType: "json",
						data: ({
							action: 'read'
						}),
						url: 'exec/exec-style-editor.php',
						success: function(data){
							myJSON = data;
						}
					});                			
				return false
				})
			});
		});              
        if (input) {
            if (esc) {
                document.execCommand('undo', false, null);
                el.blur();
            } 
        }
    }, true);
	/* Inspecteur */
	function inspectMe(){
		//e.preventDefault();
		(function(document) {
			var last;		
			function cssPath(el) {
				var //fullPath    = 0,  // 1 to build full CSS-path, 
				    // or 0 for optimised Définie par user plus bas
   					useNthChild = 1,  // 1 to use ":nth-child()" pseudo-selectors to match element
					cssPathStr = '',
					testPath = '',
					parents = [],
					parentSelectors = [],
					tagName,
					cssId,
					cssClass,
					tagSelector,
					vagueMatch,
					nth,
					i,
					c;
				while ( el ) {
					vagueMatch = 0;
					tagName = el.nodeName.toLowerCase();
					cssId = ( el.id ) ? ( '#' + el.id ) : false;
					cssClass = ( el.className ) ? ( '.' + el.className.replace(/\s+/g,".") ) : '';
					if ( cssId ) {
						tagSelector = tagName + cssId + cssClass;
					} else if ( cssClass ) {
						tagSelector = tagName + cssClass;
					} else {
						vagueMatch = 1;
						tagSelector = tagName;
					}
					parentSelectors.unshift( tagSelector )
					if ( cssId && !fullPath )
						break;
					el = el.parentNode !== document ? el.parentNode : false;
				} 		
				for ( i = 0; i < parentSelectors.length; i++ ) {
					cssPathStr += '' + parentSelectors[i] +' ' ;
					if ( useNthChild && !parentSelectors[i].match(/#/) && !parentSelectors[i].match(/^(html|body)$/) ) {			
						if ( !parentSelectors[i].match(/\./) || $( cssPathStr ).length > 1 ) {				
							for ( nth = 1, c = el; c.previousElementSibling; c = c.previousElementSibling, nth++ );					
							cssPathStr += ":nth-child(" + nth + ")";
						}
					}		
				}
				cssPathStr.replace(/^[ \t]+|[ \t]+$/, '');
				//var sol = (cssPathStr.length + 2);  What
				cssPathStr += '{\n\n}';
				$("#editable-style").focus();		
				return cssPathStr;
			}
			function inspectorMouseOver(e) {
				var element = e.target;
				element.style.outline = '1px dashed #f00';	
				element.style.background = 'rgba(0,0,0,.05)';	
				last = element;
			}
			function inspectorMouseOut(e) {
				e.target.style.outline = '';
				e.target.style.background = '';				
			}
			function inspectorOnClick(e) {	
				if ( $(e.target).parents("#zandbug").length == 1 ) {
					e.preventDefault(); 
					alert ("Veuillez choisir un élément de la page !");
					} else {
					e.preventDefault();
				$('.inspector-io').removeClass("outlined");	
				$('#editable-style').append(cssPath(e.target));		
				//console.log( cssPath(e.target) );
					document.removeEventListener("mouseover", inspectorMouseOver, true);
					document.removeEventListener("mouseout", inspectorMouseOut, true);
					document.removeEventListener("click", inspectorOnClick, true);
					document.removeEventListener("keydown", inspectorCancel, true);			
					last.style.outline = '';
					last.style.background = '';
				return false;
				}				
			}		
			function inspectorCancel(e) {
				if (e === null && event.keyCode === 27) {
					document.detachEvent("mouseover", inspectorMouseOver);
					document.detachEvent("mouseout", inspectorMouseOut);
					document.detachEvent("click", inspectorOnClick);
					document.detachEvent("keydown", inspectorCancel);
					last.style.outlineStyle = '0';
					last.style.backgroundStyle = '0';
				} else if(e.which === 27) {
					document.removeEventListener("mouseover", inspectorMouseOver, true);
					document.removeEventListener("mouseout", inspectorMouseOut, true);
					document.removeEventListener("click", inspectorOnClick, true);
					document.removeEventListener("keydown", inspectorCancel, true);			
					last.style.outline = '0';
					last.style.background = '0';
				}
			}
			if ( document.addEventListener ) {
				document.addEventListener("mouseover", inspectorMouseOver, true);
				document.addEventListener("mouseout", inspectorMouseOut, true);
				document.addEventListener("click", inspectorOnClick, true);
				document.addEventListener("keydown", inspectorCancel, true);
			} else if ( document.attachEvent ) {
				document.attachEvent("mouseover", inspectorMouseOver);
				document.attachEvent("mouseout", inspectorMouseOut);
				document.attachEvent("click", inspectorOnClick);
				document.attachEvent("keydown", inspectorCancel);
			}	
		})(document);
	};	
    /* Créer cookie What ?*/
    function createCookie(name, value, days) {
        var expires;
        if (days) {
            var date = new Date();
            date.setTime(date.getTime()+(days*24*60*60*1000));
            expires = "; expires="+date.toGMTString();
        }else {
            expires = "";
        }
        document.cookie = name+"="+value+expires+"; path=/";
    }
    /* Lire cookie What ?*/
    function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1,c.length);
            }
             if (c.indexOf(nameEQ) === 0) {
                return c.substring(nameEQ.length,c.length);
            }
        }
        return null;
    }
    /* Nicker cookie What ?*/
    function eraseCookie(name) {
        createCookie(name,"",-1);
    }
	/* BOUTONS */
	// Chemin complet des CSS
	var fullPath=0; 
	$("#full-path").click(function() {
		if (fullPath==0){
            fullPath=1; 
        }else{
            fullPath=0;
        }
    });	
	/* Activer ZBoxon */
	$("#help").click(function(e){
	     $("#help").toggleClass("helping");
        $("a .yourSelector").toggleClass("specialborderClass");
	});	
	$('#zandbug-io').click(function(e){
		e.preventDefault();
        createCookie("cookie-zb", "showzb", 30);
		$('#zandbug').removeClass("hide");
		$('#fixFrame').removeClass("hide");
		$(this).addClass("hide");
	});
	/* Désactiver ZBoxon */
 	$('#zandbug-close').click(function(e){
		e.preventDefault();
		$('#zandbug').addClass("hide");
		$('#fixFrame').addClass("hide");
		eraseCookie("cookie-zb");
		$('#zandbug-io').removeClass("hide");
	});
	/* Activer/désactiver cookie */
    var activeZb = readCookie("cookie-zb");
    if (activeZb == "showzb") {
        $('#zandbug').removeClass("hide");
        $('#zandbug-io').addClass("hide");
            
        } else {
        $('#zandbug').addClass("hide");
        $('#zandbug-io').removeClass("hide");
    }
    /* Activer l'inspecteur */  			
	$('#inspector-io').click(function(e){
		e.preventDefault();
		$('.inspector-io').addClass("outlined");
		inspectMe();
	});
	/* Ecrire couleur */     
	$('#go-color').click(function(event){
		event.preventDefault();
    	var pos = $("#editable-style").text().length -2;
    	console.log(pos);
    	// select a range of text  =  $('#elem').selectRange(3,5); 
        // set cursor position 
        $('#elem').selectRange(pos); 
    	var textAreaTxt = jQuery("#editable-style").text();
    	var txtToAdd = "color: " + $('#min-perso-c').val() + ";";
    	$("#editable-style").html(textAreaTxt.substring (0,pos) + txtToAdd + textAreaTxt.substring(pos));	
		$("#editable-style").focus();
	});
	/* Ecrire couleur de fond */  
	$('#go-background').click(function(event){
		event.preventDefault();
    	var pos = $("#editable-style").text().length -2;
    	// select a range of text  =  $('#elem').selectRange(3,5); 
        // set cursor position
        $('#elem').selectRange(pos); 
    	var textAreaTxt = jQuery("#editable-style").text();
    	var txtToAdd = "background-color: " + $('#min-perso-c').val() + ";";
    	$("#editable-style").html(textAreaTxt.substring (0,pos) + txtToAdd + textAreaTxt.substring(pos));	
		$("#editable-style").focus();
	});	
	/* Ecrire couleur de border */
	$('#go-border').click(function(event){
		event.preventDefault();
    	var pos = $("#editable-style").text().length -2;
    	// select a range of text  =  $('#elem').selectRange(3,5); 
        // set cursor position
        $('#elem').selectRange(pos); 
    	var textAreaTxt = jQuery("#editable-style").text();
    	var txtToAdd = "border:1px solid " + $('#min-perso-c').val() + ";";
    	$("#editable-style").html(textAreaTxt.substring (0,pos) + txtToAdd + textAreaTxt.substring(pos));	
		$("#editable-style").focus();
	});
	/*  Efface le style actuel */  	
	$('#no-style').click(function(event){
		event.preventDefault();
    	$("#editable-style").html(" ");
	});		
	/*  Limiter les copy, paste, cut, drop, focus, blur, keypress, input, textInput, DOMNodeInserted et tout le collier de perles */
	$('#editable-style').bind("paste drop DOMNodeInserted",function(e) {
        e.preventDefault();
    });

	/* Ecrire couleur de border */
	$('.tab-zb').addClass('clearfix').not(':first').hide();
	$('ul.tabs-zb').each(function(){
		var current = $(this).find('li.current');
		if(current.length < 1) { $(this).find('li:first').addClass('current'); }
		current = $(this).find('li.current a').attr('href');
		$(current).show();
	});
	$('ul.tabs-zb a[href^="#"]').on('click', function(e){
		e.preventDefault();
		var tabs = $(this).parents('ul.tabs-zb').find('li');
		var tab_next = $(this).attr('href');
		var tab_current = tabs.filter('.current').find('a').attr('href');
		$(tab_current).hide();
		tabs.removeClass('current');
		$(this).parent().addClass('current');
		$(tab_next).show();
		//history.pushState( null, null, window.location.search + $(this).attr('href') );
		return false;
	});
    	var wantedTag = window.location.hash;
    	if (wantedTag != ""){
        	var allTabs = $("ul.tabs-zb a[href^=" + wantedTag + "]").parents('ul.tabs-zb').find('li');
        	var defaultTab = allTabs.filter('.current').find('a').attr('href');
        	$(defaultTab).hide();
        	allTabs.removeClass('current');
        	$("ul.tabs-zb a[href^=" + wantedTag + "]").parent().addClass('current');
        	$("#" + wantedTag.replace('#','')).show();
    	}
});
/* Resizer gére les divs… resizables */
function reSizer(div) {
    const element = document.querySelector(div);
    const resizers = document.querySelectorAll(div + ' .resizer')
    const minimum_size = 20;
    let original_width = 0;
    let original_height = 0;
    let original_x = 0;
    let original_y = 0;
    let original_mouse_x = 0;
    let original_mouse_y = 0;
    for (let i = 0; i < resizers.length; i++) {
        const currentResizer = resizers[i];
        currentResizer.addEventListener('mousedown', function(e) {
            e.preventDefault()
            original_height = parseFloat(getComputedStyle(element, null).getPropertyValue('height').replace('px', ''));
            original_y = element.getBoundingClientRect().top;
            original_mouse_x = e.pageX;
            original_mouse_y = e.pageY;
            window.addEventListener('mousemove', resize)
            window.addEventListener('mouseup', stopResize)
        })

        function resize(e) {
            if (currentResizer.classList.contains('bottom-top')) {
                const height = original_height + (e.pageY - original_mouse_y)
                if (height > minimum_size) {
                    element.style.height = height + 'px'
                }
            } else {
                const height = original_height - (e.pageY - original_mouse_y)
                if (height > minimum_size) {
                    element.style.height = height + 'px'
                    element.style.top = original_y + (e.pageY - original_mouse_y) + 'px'
                }
            }
        }

        function stopResize() {
            window.removeEventListener('mousemove', resize)
        }
    }
}
reSizer('.resizer');

/*   Désastre à voir  */
function resizEl(perRes, enfRes, newSiz) {
	perRes = document.getElementById('zandbug').clientHeight;
	enfRes = document.getElementById('editable-style').clientHeight;
	goRes = document.getElementById('editable-style');	
	myFrame = document.getElementById('fixFrame');
	const curRes = document.getElementById('zandbug');
	
	curRes.addEventListener('mouseup', function(e) {
		e.preventDefault()
		nowperRes = document.getElementById('zandbug').clientHeight;

		// percentRaise = parseFloat(perRes / nowperRes);
		// newSiz = enfRes * percentRaise;
	
		goRes.setAttribute("style","height:" + nowperRes +"px");
		myFrame.setAttribute("style","height:" + nowperRes +"px"); 
		
	});
}
resizEl();