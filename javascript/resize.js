    "use strict";
    var minWidth = 60;
    var minHeight = 20;
    var FULLSCREEN_MARGINS = 10;
    var MARGINS = 10;
    var clicked = null;
    var onTopEdge,onUp;
    var b, x, y;
    var redraw = false;
    var pane = document.getElementById('zandbug');
    var nothere = document.getElementById('zandbugnot'); 
    function setBounds(element, x, y, w, h) {
        element.style.left = 0 + 'px';
        element.style.top = y + 'px';
        element.style.width = 0 + 'px';
        element.style.height = h + 'px';
    }
    pane.addEventListener('mousedown', onMouseDown);
    document.addEventListener('mousemove', onMove);
    document.addEventListener('mouseup', onUp);
    function onMouseDown(nothere) {
        onDown(e);
        e.preventDefault();
    }
    function onDown(e) {
        document.querySelector('body').className += ' noselect';
        calc(e);
        var isResizing =  onTopEdge ;
        clicked = {
            x: x,
            y: 15,
            cx: e.clientX,
            cy: e.clientY,
            w: b.width,
            h: b.height,
            isResizing: isResizing,
            isMoving: isResizing && canMove(),
            onTopEdge: onTopEdge
        };
    }
    function canMove() {
        return 
    }
    function calc(e) {
        b = pane.getBoundingClientRect();
        x = e.clientX - b.left;
        y = e.clientY - b.top;
        onTopEdge = y < MARGINS;
    }
    var e;
    function onMove(ee) {
        calc(ee);
        e = ee;
        redraw = true;
    }
    function animate() {
        requestAnimationFrame(animate);
        if (!redraw) return;
        redraw = false;
        if (clicked && clicked.onTopEdge && clicked.isResizing) {
            var currentHeight = Math.max(clicked.cy - e.clientY + clicked.h, minHeight);
            if (currentHeight > minHeight) {
                pane.style.height = currentHeight + 'px';
                pane.style.top = e.clientY + 'px';
            }
            return;
        }
        if (onTopEdge) {
            pane.style.cursor = 'ns-resize';
        } else if (canMove()) {
            pane.style.cursor = 'move';
        } else {
            pane.style.cursor = 'default';
        }
    }
    animate();
    function onUp(e) {
        calc(e);
        if (clicked && clicked.isMoving) {
           if (b.top < MARGINS) {
                setBounds(pane, 0, 0, window.innerWidth, window.innerHeight / 2);
            } 
        }
        clicked = null;
    }
    
    
    
 /*
 
This just solves the problem of text selection.
 e.preventDefault();

$("#inputform").mousedown(function(e){
    e.preventDefault();
    //console.log('mousedown');
}).mouseup(function(e){
    e.preventDefault();
    //console.log('mouseup');
});



•••••••••••••••••••••••••••••

Peut être utile pour faire readthis functionner unes seule fois

var yourFunction = function () {
  // do stuff
  yourFunction = function() {return false;}
}; 
 
 
 
Autre belle piste pour read this 



$(window).on("scroll resize", function(){
    var pos=$('#date').offset();
    $('.post').each(function(){
        if(pos.top >= $(this).offset().top && pos.top <= $(this).next().offset().top)
        {
            $('#date').html($(this).html()); //or any other way you want to get the date
            return; //break the loop
        }
    });
});

$(document).ready(function(){
  $(window).trigger('scroll'); // init the value
});
 
.post {border: 1px dashed grey; margin-right: 50px; margin-bottom: 20px;}
#date {position:fixed; top:10px; right:0px; height: 90px; width: 45px; background: grey;} 



<div style='height:200px;' class='post'>One date</div>
<div style='height:250px;' class='post'>Another date</div>
<div style='height:350px;' class='post'>And another date</div>
<div style='height:200px;' class='post'>And one more date</div>
<div id='date'></div>
<div style='height:800px;'></div>

 
 
••••••••••••••••••••••••••••• 
 
The HTML5 Placeholder property is a totally awesome way to label forms, but as a new feature, support for it varies. It’s occasionally difficult to style placeholder text as well. This jQuery function imitates the behavior you’d expect, using the value property of the input tag:

    jQuery(document).ready(function() {
      jQuery.fn.cleardefault = function() {
        return this.focus(function() {
        if( this.value == this.defaultValue ) {this.value = “”;}
      }).blur(function() {
        if( !this.value.length ) {this.value = this.defaultValue;}
      });
    };
    jQuery(“input, textarea”).cleardefault();

    });
    
•••••••••••••••••••••••••••••    
      
Scroll Events

Set up an event listener using jQuery that updates sidebar content with a date:

    $(window).on(“scroll resize”, function(){
      var pos=$(‘#date’).offset();
      $(‘.post’).each(function(){
      if(pos.top >= $(this).offset().top && pos.top <= $(this).next().offset().top) {
        $('#date').html($(this).html());
        return; //break the loop
      }
      });
    });

    $(document).ready(function(){
    $(window).trigger('scroll'); // init the value
    }); 
    
•••••••••••••••••••••••••••••    

GET URL variables and return them as an "associative array."

Calling the function while at example.html?foo=asdf&bar=jkls sets map['foo']='asdf' and map['bar']='jkls'


    function getUrlVars() {
    var map = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
    map[key] = value;
    });
    return map;
    }

 •••••••••••••••••••••••••••
 
A special action when double-clicked which prevents the default action

Double-click: does something special
Click: works as normal

You'll need to have a very slight delay on firing off the normal click action, which you cancel when the double click event happens.

function doClickAction() {
  $("#click h2").append("•"); 
}
function doDoubleClickAction() {
  $("#double-click h2").append("•"); 
}

var timer = 0;
var delay = 200;
var prevent = false;

$("#target")
  .on("click", function() {
    timer = setTimeout(function() {
      if (!prevent) {
        doClickAction();
      }
      prevent = false;
    }, delay);
  })
  .on("dblclick", function() {
    clearTimeout(timer);
    prevent = true;
    doDoubleClickAction();
  });      
 
•••••••••••••••••••••••••••••

JavaScript can access the current URL in parts. For this URL:

http://css-tricks.com/example/index.html

    window.location.protocol = "http:"
    window.location.host = "css-tricks.com"
    window.location.pathname = "example/index.html"

So to get the full URL path in JavaScript:

var newURL = window.location.protocol + "//" + window.location.host + "/" + window.location.pathname;

If you need to breath up the pathname, for example a URL like http://css-tricks.com/blah/blah/blah/index.html, you can split the string on "/" characters

var pathArray = window.location.pathname.split( '/' );

Then access the different parts by the parts of the array, like

var secondLevelLocation = pathArray[0];

To put that pathname back together, you can stitch together the array and put the "/"'s back in:

var newPathname = "";
for (i = 0; i < pathArray.length; i++) {
  newPathname += "/";
  newPathname += pathArray[i];
}



•••••••••••••••••••••••••••••

Les deux suivants c'est pour hover une part du div resize

$('#trigger').hover(function(){
    $(this).parent().animate({'top':0},500); 
});
$('#slider').mouseleave(function(){
    $(this).animate({'top':-150},500); 
});
•••••••••••••••••••••••••••••

$("#div").mousemove(function(e)
{
    var x = e.pageX - this.offsetLeft;
    var y = e.pageY - this.offsetTop;
    // et ici il faut un if hover top is > 40
        // do something
  
  
   $('#special').html(x +', '+ y);

    
    
    
});
•••••••••••••••••••••••••••••
•••••••••••••••••••••••••••••
••••••••••••••••••••••••••••• 
 */   
    
    
    
    
    
    
    
    
    
    
    
    