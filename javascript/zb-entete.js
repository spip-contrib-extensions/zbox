jQuery(document).ready(function($){
	"use strict";
	// Editor vars	•••••••••••••••••••••••••••••••••••••••		
	/*
	if (typeof console  != "undefined") 
	if (typeof console.log != 'undefined')
		console.olog = console.log;
	else
		console.olog = function() {};
		console.log = function(message) {
			console.olog(message);
			$('#editable_style').empty();
			$('#editable_style').append(message + '<br />');
			
			// Tentative count
			//Array.prototype.count = function() {
			//	return this.length;
			//};
			
		};
	console.error = console.debug = console.info =  console.log;
	*/
	$('#editable_style').click(function(event){
		event.stopPropagation();
		//document.body.addEventListener('drop',drop,false);  ???? Ça doit venir de la console
		$(this).css('cursor','text');
		$(this).focus();	
	});
	
	$('#editable_style').bind('paste', function(){ // catch the paste-event in the DIV
        // get content before paste
        var before = document.getElementById('editable_style').innerHTML;
        setTimeout(function(){
            // get content after paste by a 100ms delay
            var after = document.getElementById('editable_style').innerHTML;
            // find the start and end position where the two differ
            var pos1 = -1;
            var pos2 = -1;
            for (var i=0; i<after.length; i++) {
                if (pos1 == -1 && before.substr(i, 1) != after.substr(i, 1)) pos1 = i;
                if (pos2 == -1 && before.substr(before.length-i-1, 1) != after.substr(after.length-i-1, 1)) pos2 = i;
            }
            // the difference = pasted string with HTML:
            var pasted = after.substr(pos1, after.length-pos2-pos1);
            // strip the tags:
            var replace = pasted.replace(/<[^>]+>/g, '');
            // build clean content:
            var replaced = after.substr(0, pos1)+replace+after.substr(pos1+pasted.length);
            // replace the HTML mess with the plain content
            document.getElementById('editable_style').innerHTML = replaced;
            }, 100);
        });
        
        
    $("#top_h1_size").on("change", function() {
        $("#top_h1").css("font-size", $(this).val() + "rem");
	});
	
	$("#logo_size").on("change", function() {
		$("#logo").css("width", $(this).val() + "%");
	});

	$("#min_top_00").on("change", function() {
		$("#c_1").css("color", $(this).val());
	});
    
	$("#min_top_bis").on("change", function() {
		$("#c_2").css("color", $(this).val());
	});
	
 	$("#min_top_href").on("change", function() {
		$("#c_3").css("color", $(this).val());
	});
       
 	$("#nor_top_10").on("change", function() {
		$(".zone_t").css("border-bottom-width", $(this).val())  + "px; ";
	});
       
 	$("#nor_top_20").on("change", function() {
		$("#zone_t").css("border-bottom-style", $(this).val())  + "; ";
	});
        
 	$("min_top_30").on("change", function() {
		$(".zone_t").css("border-bottom-color", $(this).val())  + "; ";
	});
        
			
});	