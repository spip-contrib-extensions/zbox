# ZBox

Un squelette pour SPIP très expérimental



Ce n’est pas sans appréhension que je publie ce projet. Je crois qu’il a des potentialités, mais je ne suis pas sûr de la qualité, ou plutôt oui, à condition de le considérer comme une maquette.

Il essaie de tirer les avantages de **CSP**. Pour cela, il faut installer les htaccess, un à la racine et l’autre dans écrire. Les différents audits **CSP** (*Google*, *Mozilla* et *Security Headers* sont plutôt bienveillants).

**Google Page Speed** est content, aussi. J’ai 100 partout. Tout le javascript se charge en bas de page, par deferred. Faut dire que c’est un pompage brutal de z-core, mais en ajoutant bodys dans les inclure appelés par structure. 
Ainsi on a : `“bodys”`, `“content”`, `“nav”`, `“extra”`, `“extras”`, 
`“head”`, `“head_js”`, `“header”`, `“footer”` et `“deferred”`. 
C’est ce dernier qui charge les js par page.

J’utilise pleeease pour les css, (le pleeease cli, que me génère les css spécifiques pour les brouteurs). Si vous ne voulez pas l’utiliser, in faut renommer `_in.css` en `_out.css` et ça devrait suffire. Voir `http://pleeease.io/`

L’objectif serait d’avoir un squelette facile d’accès, modifiable à la volée par des néophytes. J’ai essayé d’avoir des css plutôt simples, jouant avec des classes génériques (regardez dans base.css pour avoir une idée).

Il y a beaucoup de trucs en dur, ce n’est pas “publiable” dans l’état. Je le mets ici pour voir si ça intéresse quelqu’un. Inutile de dire que je suis à votre disposition. J’espère que ça va plaire et pouvoir retourner un peu du plaisir que SPIP m’a donné.

### TODO

Ils sont légion, c’est sûr… Je butte néanmoins sur

* Rédiger (générer) une doc automatique pour les css et les js 
* Utiliser les workers 
* Le découpage des squelettes à fignoler (et à comprendre) 
* Trouver une manière pour que les media-queries gèrent les #INCLURE 
* Différents layouts selon colonnes 
* Remplacer la dépendence de UPLOAD HTML5 par un upload tout con (je n’arrive pas) 
* Création de balise sans Champs extras (je n’arrive pas)

Sinon le plugin a cet aspect une fois installé…

![Public](img/ZB_public.png)

![Privé](img/ZB_prive.png)


