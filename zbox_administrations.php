<?php
if (!defined('_ECRIRE_INC_VERSION')) return;
function zbox_upgrade($nom_meta_base_version, $version_cible){
	$maj = array();
	$maj['create'] = array(
		array('maj_tables', array('spip_zbox', 'spip_zbox_liens')),
	);
	/*
	// id_article
	$maj['1.2.0'] = array(
		array('maj_tables', array('spip_zbox')),
		array('sql_alter',  "TABLE spip_zbox ADD INDEX id_article(id_article)")
	);
	// lang, langue_choisie
	$maj['1.3.0'] = array(array('maj_tables', array('spip_zbox')));
	// id_trad
	$maj['1.4.0'] = array(array('maj_tables', array('spip_zbox')));
	// statut
	$maj['1.5.0'] = array(
		array('maj_tables', array('spip_zbox')),
		array('sql_updateq', 'spip_zbox', array('statut'=>'publie'))
	);
	*/
	// spip_zbox_liens
	$maj['1.6.0'] = array(array('maj_tables', array('spip_zbox_liens')));
	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}
function zbox_vider_tables($nom_meta_base_version) {
	sql_drop_table("spip_zbox");
	sql_drop_table("spip_zbox_liens");
	effacer_meta($nom_meta_base_version);
}
