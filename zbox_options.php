<?php
/**
 * Options au chargement du plugin ZBox
 *
 * @plugin     ZBox
 * @copyright  2019
 * @author     Luis Speciale
 * @licence    GNU/GPL
 * @package    SPIP\Zbox\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

	/* GLOBALS chargés à chaque hit */
	$GLOBALS['csvimport_tables_jointures'] = true;
	$GLOBALS['dossier_squelettes']='plugins/zbox/prive/squelettes';
	$GLOBALS['dossier_inclure']='plugins/zbox/prive/inclure';
	$GLOBALS["toujours_paragrapher"]=false;
	$GLOBALS["z_blocs"] = array("bodys","content","nav","extra","extras","head","head_js","header","footer","deferred");
	define("_DIR_THEMES",_DIR_RACINE."plugins/zbox/themes/");
	define ("CONTEXTE_ANCRES_DOUCES",".ancres_douces");
	//define ("_LOG_FILTRE_GRAVITE",8);
	//define ("_NO_CACHE",-1); 	//-1,0,1 
	//define ("_INTERDIRE_COMPACTE_HEAD_ECRIRE",true); 	// 	false,true 	Désactiver les caches CSS et Javascript
	//define ("_LOG_FILELINE",true); 	// fichier, ligne, nom de la fonction
	
	/* Pompé de ZCore */
	define('_DEFINIR_CONTEXTE_TYPE_PAGE', true);
	define('_ZPIP', true);
	if ($z = _request('var_zajax') AND !preg_match(",[^\w-],", $z)) {
		if (!isset($GLOBALS['marqueur'])) {
			$GLOBALS['marqueur'] = "$z:";
		} else {
			$GLOBALS['marqueur'] .= "$z:";
		}
		$GLOBALS['flag_preserver'] = true;
	} else {
		set_request('var_zajax', '');
	}
	if (!isset($GLOBALS['spip_pipeline']['recuperer_fond'])) $GLOBALS['spip_pipeline']['recuperer_fond'] = '';
	$GLOBALS['spip_pipeline']['recuperer_fond'] .= '||zbox_recuperer_fond';

	// Ajouter la langue d'origine dans le contexte systematiquement.
	if (!$langue = _request('lang')) {
		include_spip('inc/lang');
		$langues = explode(',', $GLOBALS['meta']['langues_multilingue']);
		// si la langue est definie dans l'url (en/ ou fr/) on l'utilise
		if (preg_match(',^' . $GLOBALS['meta']['adresse_site'] . '/(' . join('|',$langues) . ')/,', 'http://' . $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'], $r)) {
			$langue = $r[1];
			changer_langue($langue);
		} else {	
			$langue = utiliser_langue_visiteur();
			if (!in_array($langue, $langues)) {
				//$langue = "en"; // pour ne pas s'embeter !
				$langue = $GLOBALS['meta']['langue_site'];
			}
		}
		// Stocker dans $_GET
		set_request('lang', $langue);
	} 
	// Stocker langue en cookie...
	if ($langue != $_COOKIE['spip_lang']) {
		include_spip('inc/cookie');
		spip_setcookie('spip_lang', $langue);
	}	
	/* JQuery ratatiné */
	function f_jQuery_dist($texte) {
		$x = '';
		$jquery_plugins = pipeline('jquery_plugins',
			array(
				'javascript/jquery.js',
				'javascript/jquery-migrate-3.0.1.js',
				'javascript/jquery.form.js',
				'javascript/jquery.autosave.js',
				'javascript/jquery.placeholder-label.js',
				'javascript/ajaxCallback.js',
				'javascript/js.cookie.js',
				'javascript/jquery.cookie.js'
			));
		foreach (array_unique($jquery_plugins) as $script) {
			if ($script = find_in_path(supprimer_timestamp($script))) {
				$script = timestamp($script);
				$x .= "\n<script src=\"$script\" type=\"text/javascript\"></script>\n";
			}
		}
		$texte = $x . $texte;
		return $texte;
	}

	/*	Une fonction récursive pour afficher #ENV, #GET, #SESSION
	 *		en squelette : [(#ENV|bel_env)], [(#GET|bel_env)], [(#SESSION|bel_env)]
	 *		ou encore [(#ARRAY{0,1, a,#SESSION, 1,#ARRAY{x,y}}|bel_env)]
	 * @param 	string|array 		$env
	 *		si une string est passée elle doit être le serialize d'un array 
	 * @param 	bool         		$afficher_en_clair
	 *      si vrai indique qu'il faut afficher la chaine vide, la valeur null
	 *      et les booleens respectivement comme `''`, `null`, `true` ou `false`.
	 * @return string
	 *		une chaîne html affichant une <table>
	 */
	function bel_env($env, $afficher_en_clair = false) {
		if (!$afficher_en_clair) {
			$env = str_replace(array('&quot;', '&#039;'), array('"', '\''), $env);
		}
		if (is_array($env_tab = @unserialize($env))) {
			$env = $env_tab;
		}
		if (!is_array($env)) {
			return '';
		}
		$style = "";
		$res = "<table class='bel_env'>\n";
		foreach ($env as $nom => $val) {
			if (is_array($val) || is_array(@unserialize($val))) {
				$val = bel_env($val, $afficher_en_clair);
			}
			elseif (($val === null) and $afficher_en_clair) {
				$val = '<i>null</i>';
			}
			elseif (($val === '') and $afficher_en_clair) {
				$val = "<i>''</i>";
			}
			elseif (($val === true) and $afficher_en_clair) {
				$val = '<i>true</i>';
			}
			elseif (($val === false) and $afficher_en_clair) {
				$val = '<i>false</i>';
			}
			else {
				$val = entites_html($val);
			}
			$res .= "<tr>\n<td$style><strong>". entites_html($nom).
					"&nbsp;:&nbsp;</strong></td><td$style>" .$val. "</td>\n</tr>\n";
		}
		$res .= "</table>";
		return $res;
	}
