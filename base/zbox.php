<?php
if (!defined('_ECRIRE_INC_VERSION')) return;
function zbox_declarer_tables_objets_sql($tables){
	$tables['spip_zbox'] = array(
		'principale' => "oui",
		'field'=> array(
			"id_zbox"        => "bigint(21) NOT NULL",
			//"id_article"     => "bigint(21) NOT NULL DEFAULT 0",
			"nom"            => "tinytext DEFAULT '' NOT NULL",
			//"infos"          => "tinytext DEFAULT '' NOT NULL",
			"infos"          => "text DEFAULT '' NULL",
			"date"           => "datetime DEFAULT '0000-00-00 00:00:00' NOT NULL",
			"maj"            => "TIMESTAMP"
		),
		'key' => array(
			"PRIMARY KEY"    => "id_zbox",
			//"KEY id_article" => "id_article",
		),
		'titre' => "nom AS titre, '' AS lang",
		'date' => "date",
		'champs_editables' => array(
			"nom", "infos"
		),
		'champs_versionnes' => array(
			"nom", "infos" 
		),
		'rechercher_champs' => array(
			'nom' => 8, 'infos' => 2
		),
		'tables_jointures' => array(
			'zbox_liens'
		),
	);
	return $tables;
}
function zbox_declarer_tables_auxiliaires($tables) {
	$tables['spip_zbox_liens'] = array(
		'field' => array(
			"id_zbox"  => "bigint(21) DEFAULT '0' NOT NULL"
		),
		'key' => array(
			"PRIMARY KEY" => "id_zbox",
			"KEY id_zbox" => "id_zbox"
		)
	);
	return $tables;
}
function zbox_declarer_tables_interfaces($interfaces) {
	$interfaces['table_des_tables']['zbox'] = 'zbox';
	return $interfaces;
}


