<?php
/**
 * Définit les autorisations du plugin ZBox
 *
 * @plugin     ZBox
 * @copyright  2019
 * @author     Luis Speciale
 * @licence    GNU/GPL
 * @package    SPIP\Zbox\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/*
 * Un fichier d'autorisations permet de regrouper
 * les fonctions d'autorisations de votre plugin
 */

/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser */
function zbox_autoriser() {
	return $qui['statut'] == '0minirezo';
}

function autoriser_zbox_configurer($faire, $type, $id, $qui, $opt) {
	return $qui['statut'] == '0minirezo';
}
function autoriser_zbox_configurer_dist($faire, $type, $id, $qui, $opt) {
	return $qui['statut'] == '0minirezo';
}


/* Exemple
function autoriser_zbox_configurer_dist($faire, $type, $id, $qui, $opt) {
	// type est un objet (la plupart du temps) ou une chose.
	// autoriser('configurer', '_zbox') => $type = 'zbox'
	// au choix :
	return autoriser('webmestre', $type, $id, $qui, $opt); // seulement les webmestres
	return autoriser('configurer', '', $id, $qui, $opt); // seulement les administrateurs complets
	return $qui['statut'] == '0minirezo'; // seulement les administrateurs (même les restreints)
	// ...
}
*/
