<?php

/**
 * Ajouter les zbox sur les vues de articles
 *
 * @param 
 * @return 
*
function zbox_affiche_enfants($flux) {
	if ($e = trouver_objet_exec($flux['args']['exec'])
	  AND $e['type'] == 'article'
	  AND $e['edition'] == false) {	  
		$id_article = $flux['args']['id_article'];  	
		$bouton = '';
		if (autoriser('creerzboxdans','article', $id_article)) {
			$bouton .= icone_verticale(_T('zbox:icone_creer_zbox'), generer_url_ecrire('zbox_edit', "id_article=$id_article"), "zbox-24.png", "new", 'right')
					. "<br class='nettoyeur' />";
		}		
		$lister_objets = charger_fonction('lister_objets','inc');	
		$flux['data'] .= $lister_objets('zbox',array('titre'=>_T('zbox:titre_zbox_article') , 'id_article'=>$id_article, 'par'=>'nom'));
		$flux['data'] .= $bouton;		
	}
	return $flux;
}

function zbox_affiche_milieu($flux) {
	$texte = "";
	$e = trouver_objet_exec($flux['args']['exec']);
	// auteurs sur les zbox
	if ($e['type'] == 'zbox' AND !$e['edition']) {		
		//$texte = recuperer_fond('prive/objets/editer/liens', array(
		$texte = recuperer_fond('prive/squelettes/contenu/configurer', array(
			'table_source' => 'auteurs',
			'objet' => $e['type'],
			'id_objet' => $flux['args'][$e['id_table_objet']],
			#'editable'=>autoriser('associerauteurs',$e['type'],$e['id_objet'])?'oui':'non'
		));
	}
	// zbox sur les articles
	if ($e['type'] == 'article' AND !$e['edition']) {
		$texte = recuperer_fond('prive/objets/editer/liens', array(
			'table_source' => 'zbox',
			'objet' => $e['type'],
			'id_objet' => $flux['args'][$e['id_table_objet']],
			#'editable'=>autoriser('associerzbox',$e['type'],$e['id_objet'])?'oui':'non'
		));
	}
	if ($texte) {
		if ($p=strpos($flux['data'],"<!--affiche_milieu-->"))
			$flux['data'] = substr_replace($flux['data'],$texte,$p,0);
		else
			$flux['data'] .= $texte;
	}	
	return $flux;
}
*/
if (!defined("_ECRIRE_INC_VERSION")) return;
    function zbox_affiche_milieu($flux){
    	if ($flux['args']['exec'])
    		$flux['data'] .= recuperer_fond('prive/squelettes/contenu/configurer', array('configurer' => 'configurer_zbox'));
    	return $flux;
    }	    
    function zbox_affiche_gauche($flux){
    	if ($flux['args']['exec'])
    		$flux['data'] .= recuperer_fond('prive/squelettes/navigation/configurer', array('configurer' => 'configurer_zbox'));
    	return $flux;
    }
    