<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/actions');
include_spip('inc/editer');


function formulaires_editer_zbox_charger_dist($id_zbox='new'){
	$valeurs = formulaires_editer_objet_charger('zbox',$id_zbox);
	return $valeurs;
}

/**
 * Identifier le formulaire en faisant abstraction des parametres qui ne representent pas l'objet edite
 */
function formulaires_editer_zbox_identifier_dist($id_zbox='new'){
	return serialize(array(intval($id_zbox)));
}

function formulaires_editer_zbox_verifier_dist($id_zbox='new'){
	return formulaires_editer_objet_verifier('zbox', $id_zbox);
}

function formulaires_editer_zbox_traiter_dist($id_zbox='new'){
	return formulaires_editer_objet_traiter('zbox',$id_zbox);
}

